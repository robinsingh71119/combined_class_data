CSS (CASCADING STYLE SHEET)
CASCADING :- One Designing file can be share on many screen/files.

CSS - Types

EXTERNAL:- External file from url or store in local


INTERNAL :- Code in style tag inside head tag.

INLINE :- Code in each tag in a style attribute.


INLINE : 
style='property:value;property:value;'







--List  in HTMl
-Order list
1-An ordered list starts with the <ol> tag. Each list item starts with the <li> tag.
The Type Attribute
--It will define the bullet you have to use.
Syntax:-
    <ol type='a'> 
    <li></li>
    </ol>
The Start Attribute:-
--It will start the list with start value.
Syntax:-
    <ol start='10'> 
    <li></li>
    </ol>
List CSS style properties:-
--This property is used to change the bullet of unorder list .
Syntax:-
    <ol list-style-type:none;> 
    <li></li>
    </ol>

-Unorder list
1-An unordered list starts with the <ul> tag. Each list item starts with the <li> tag.
2-You can use list-style-type here to chage the bullet of li tag.






---Applying color in html with different ways. 
1-Color Name
2-RGB:- rgb()
3-Hexa code :- #2b2b2b2
4-RGBA:- rgba()  




---CSS Text properties
--text-tranform:-
-The text-transform property controls the capitalization of text.
Syntax:-
--text-transform: uppercase,lowercase,capitalize;
-text-decoration:-
-The text-decoration property specifies the decoration added to text.
Syntax:-
--text-decoration:underline linestyle colorname;
-The text-shadow property adds shadow to text.
--text-indent:-
The text-indent property specifies the indentation of the first line in a text-block.
--text-align:-
The text-align property specifies the horizontal alignment of text in an element.
--word-spacing:-
The word-spacing property increases or decreases the white space between words.
--letter-spacing:-  
The letter-spacing property increases or decreases the space between characters in a text.
--line-height:-
The line-height property specifies the height of a line.